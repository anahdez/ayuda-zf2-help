# Zend\ServiceManager

El patrón de diseño Servicio de localización es implementada por el componente de [Zend\ServiceManager]. El Servicio de Busqueda es un localizador de [service/object] , encargado de la recuperación de otros objetos. A continuación se presenta la API [Zend\ServiceManager\ServiceLocatorInterface]
```sh
namespace Zend\ServiceManager;
interface ServiceLocatorInterface
{
    public function get($name);
    public function has($name);
}
```

- has($name), comprueba si tiene un servicio llamado ServiceManager;
- get($name), recupera un servicio por su nombre.

El [Zend\ServiceManager\ServiceManager] es una implementación de [ServiceLocatorInterface]. Además de los métodos descritos anteriormente, el ServiceManager proporciona un API adicional:

- Registro en el Servicio. [ServiceManager::setService] le permite registrar un objeto como un servicio:

```sh
$serviceManager->setService('my-foo', new stdClass());
$serviceManager->setService('my-settings', array('password' => 'super-secret'));

var_dump($serviceManager->get('my-foo')); // una instancia de stdClass
var_dump($serviceManager->get('my-settings')); // array('password' => 'super-secret')
```
- Objetos de servicio Lazy-loaded. [ServiceManager::setInvokableClass] permite indicar al [ServiceManager] qué clase instanciar cuando se solicita un servicio en particular:

```sh
$serviceManager->setInvokableClass('foo-service-name', 'Fully\Qualified\Classname');

var_dump($serviceManager->get('foo-service-name')); // una instancia de  Fully\Qualified\Classname
```
- Fábricas de servicio. En lugar de una instancia de objeto real o un nombre de clase, puede indicar al ServiceManager que invoque una fábrica siempre con el fin de obtener la instancia de objeto. Las fábricas pueden ser tanto una devolución de llamada PHP, un objeto que implementa [Zend\ServiceManager\FactoryInterface], o el nombre de una clase que implementa la interfaz:

```sh
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MyFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new \stdClass();
    }
}

// registering a factory instance
$serviceManager->setFactory('foo-service-name', new MyFactory());

// registering a factory by factory class name
$serviceManager->setFactory('bar-service-name', 'MyFactory');

// registering a callback as a factory
$serviceManager->setFactory('baz-service-name', function () { return new \stdClass(); });

var_dump($serviceManager->get('foo-service-name')); // stdClass(1)
var_dump($serviceManager->get('bar-service-name')); // stdClass(2)
var_dump($serviceManager->get('baz-service-name')); // stdClass(3)
```

- Servicio aliasing. Con ServiceManager::setAlias puede crear alias de cualquier servicio registrada, fábrica o invocable, o incluso otros alias:

```sh
$foo      = new \stdClass();
$foo->bar = 'baz!';

$serviceManager->setService('my-foo', $foo);
$serviceManager->setAlias('my-bar', 'my-foo');
$serviceManager->setAlias('my-baz', 'my-bar');

var_dump($serviceManager->get('my-foo')->bar); // baz!
var_dump($serviceManager->get('my-bar')->bar); // baz!
var_dump($serviceManager->get('my-baz')->bar); // baz!
```

- Fábricas abstractas. Una fábrica de resumen puede ser considerada como un "retorno" de fábrica. Si el gerente de servicio no era capaz de encontrar un servicio para el nombre solicitado, se comprobará las fábricas abstractas registrados:

```sh
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\AbstractFactoryInterface;

class MyAbstractFactory implements AbstractFactoryInterface
{
    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        // this abstract factory only knows about 'foo' and 'bar'
        return $requestedName === 'foo' || $requestedName === 'bar';
    }

    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $service = new \stdClass();

        $service->name = $requestedName;

        return $service;
    }
}

$serviceManager->addAbstractFactory('MyAbstractFactory');

var_dump($serviceManager->get('foo')->name); // foo
var_dump($serviceManager->get('bar')->name); // bar
var_dump($serviceManager->get('baz')->name); // exception! Zend\ServiceManager\Exception\ServiceNotFoundException
```
- Inicializadores. Es posible que desee ciertos puntos de inyección a ser siempre llaman. A modo de ejemplo, cualquier objeto que carga a través del administrador de servicios que implementa [Zend\EventManager\EventManagerAwareInterface] probablemente debería recibir una instancia [EventManager]. Inicializadores son callbacks PHP o clases de desarrollo [Zend\ServiceManager\InitializerInterface]. Reciben la nueva instancia, y luego pueden manipularlo:

```sh
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\InitializerInterface;

class MyInitializer implements InitializerInterface
{
    public function initialize($instance, ServiceLocatorInterface $serviceLocator)
    {
        if ($instance instanceof \stdClass) {
            $instance->initialized = 'initialized!';
        }
    }
}

$serviceManager->addInitializer('MyInitializer');
$serviceManager->setInvokableClass('my-service', 'stdClass');

var_dump($serviceManager->get('my-service')->initialized); // initialized!
```
Además de lo anterior, el ServiceManager también proporciona vínculos opcionales a [Zend\Di], permitiendo [Di] para actuar como un inicializador o una fábrica abstracta para el jefe de servicio.