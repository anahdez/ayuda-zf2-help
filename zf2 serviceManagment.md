# Zend\ServiceManager inicio rápido

Por defecto, Zend Framework utiliza [Zend\ServiceManager] dentro de la capa MVC y en varios otros componentes. Como tal, en la mayoría de los casos podrás proporcionar servicios, invocar classes, aliases y factories ya sea a través de la configuración o atraves las clases de los módulos.

Por defecto, el administrador de módulos escucha a [Zend\ModuleManager\Listener\ServiceListener] y hará lo siguiente:

  - Para los módulos de aplicación [Zend\ModuleManager\Feature\ServiceProviderInterface] o el metodo [getServiceConfig()], se llamará a dicho método y combine la configuración recuperada.
  - Después de que se hayan procesado todos los módulos, se agarra a la configuración desde el registro [Zend\ModuleManager\Listener\ConfigListener] y combinar cualquier configuración en la clave service_manager.
  - Finalmente, se utilizará la configuración resultante de la fusión para configurar la instancia [ServiceManager].
  
  En la mayoría de los casos, usted no va a interactuar con el ServiceManager, aparte de la prestación de servicios a la misma; su aplicación normalmente se basará en la configuración de la ServiceManager para asegurar que los servicios están configurados correctamente con sus dependencias. Al crear fábricas, sin embargo, es posible que desee interactuar con el ServiceManager para recuperar otros servicios para inyectar como dependencias. Además, hay algunos casos en los que es posible que desee recibir el ServiceManager a lazy-recuperar dependencias; como tal, es posible que desee aplicar ServiceLocatorAwareInterface y conocer más detalles acerca de la API de la ServiceManager.

# Uso de la configuración

Configuración requiere una clave service_manager en el nivel superior de la configuración, con cualquiera de los siguientes sub-llaves:
    - Abstract_factories, que debe ser una matriz de nombres de clase fábrica abstractos.
    - Alias, que debería ser un array asociativo de alias pares nombre / nombre de destino (donde el nombre de destino también puede ser un alias).
    - Fábricas, de una gama de servicios de nombres / fábrica pares de nombres de clase. Las fábricas deben ser tanto las clases que implementan Zend \ ServiceManager \ clases invokable FactoryInterface o. Si está utilizando archivos de configuración de PHP, puede proporcionar cualquier ejecutable de PHP como la fábrica.
    - Invokables, una serie de servicios de pares nombre / nombre de clase. El nombre de la clase debe ser de clase que se pueden crear instancias directamente sin argumentos de constructor.
    - Servicios, una gran variedad de servicios de pares nombre / objeto. Claramente, esto sólo funcionará con la configuración de PHP.
    - Compartida, un conjunto de pares de nombre de servicio / booleanos, indicando si es o no un servicio debe ser compartida. Por defecto, el ServiceManager asume todos los servicios son compartidos, pero puede especificar un valor falso booleano para indicar una nueva instancia debe ser devuelto.

# Módulos como Proveedores de Servicios

Los módulos pueden actuar como proveedores de servicio de configuración. Para ello, la clase de módulo debe aplicar bien [Zend\ModuleManager\Feature\ServiceProviderInterface] o simplemente el método getServiceConfig () (que también se define en la interfaz). Este método debe devolver uno de los siguientes:
    - Una matriz (u objeto de Traversable) de configuración compatible con [Zend\ServiceManager\Config]. (Básicamente, debe tener las claves para la configuración como se discutió en la sección anterior.
    - Una cadena que proporciona el nombre de una clase que implementa [Zend\ServiceManager\ConfigInterface].
    - Una instancia de cualquiera [Zend\ServiceManager\Config] , o un objeto que implementa [Zend\ServiceManager\ConfigInterface].

Como se señaló anteriormente, esta configuración se fusionará con el retorno de la configuración de otros módulos, así como archivos de configuración, antes de ser enviado al ServiceManager; esto permite la configuración  primordial a partir de módulos fácilmente.

# Ejemplos
## Ejemplo de configuración

Lo siguiente es la configuración válida para cualquier configuración que se fusionó en su aplicación, y demuestra cada una de las posibles claves de configuración. La configuración se fusionó en el siguiente orden:

- Configuración de regresar de clases del módulo a través del método getServiceConfig (), en el orden en que se procesan los módulos.
- La configuración del módulo en la clave service_manager, en el orden en que se procesan los módulos.
- Configuración de la aplicación en el directorio [config/autoload/], en el orden en que se procesan.

Como tal, tu tienes una variedad de maneras para anular los ajustes de configuración del gestor de servicios.

```sh
<?php
// a module configuration, "module/SomeModule/config/module.config.php"
return array(
    'service_manager' => array(
        'abstract_factories' => array(
            // Valid values include names of classes implementing
            // AbstractFactoryInterface, instances of classes implementing
            // AbstractFactoryInterface, or any PHP callbacks
            'SomeModule\Service\FallbackFactory',
        ),
        'aliases' => array(
            // Aliasing a FQCN to a service name
            'SomeModule\Model\User' => 'User',
            // Aliasing a name to a known service name
            'AdminUser' => 'User',
            // Aliasing to an alias
            'SuperUser' => 'AdminUser',
        ),
        'factories' => array(
            // Keys are the service names.
            // Valid values include names of classes implementing
            // FactoryInterface, instances of classes implementing
            // FactoryInterface, or any PHP callbacks
            'User'     => 'SomeModule\Service\UserFactory',
            'UserForm' => function ($serviceManager) {
                $form = new SomeModule\Form\User();

                // Retrieve a dependency from the service manager and inject it!
                $form->setInputFilter($serviceManager->get('UserInputFilter'));
                return $form;
            },
        ),
        'invokables' => array(
            // Keys are the service names
            // Values are valid class names to instantiate.
            'UserInputFiler' => 'SomeModule\InputFilter\User',
        ),
        'services' => array(
            // Keys are the service names
            // Values are objects
            'Auth' => new SomeModule\Authentication\AuthenticationService(),
        ),
        'shared' => array(
            // Usually, you'll only indicate services that should **NOT** be
            // shared -- i.e., ones where you want a different instance
            // every time.
            'UserForm' => false,
        ),
    ),
);
```
> Nota: Configuración y PHP, Por lo general, usted no debe tener los archivos de configuración crean nuevas instancias de objetos o incluso cierres de fábricas; en el momento de la configuración, no toda la carga automática puede estar en su lugar, y si otra configuración sobrescribe éste más tarde, estás ahora pasar de la CPU y la memoria de realizar el trabajo que se pierde en última instancia. Para los casos que requieren fábricas, escribir una fábrica. Si desea inyectar, instancias configuradas específicos, utilice la clase del módulo de hacerlo, o un oyente. Además, perderá la capacidad de utilizar la función de almacenamiento en caché de los archivos de configuración cuando se utiliza el cierre dentro de ellos. Esta es una limitación de PHP que no se (de) serializar cierres.

> Nota - Nombres de servicio, buenas prácticas.
> Al definir un nuevo servicio, por lo general es una buena idea utilizar el nombre completo de la clase de la instancia producido o de una de las interfaces que implementa como nombre del servicio. El uso de un FQCN como nombre de servicio hace colisiones con otros servicios muy difícil si la clase es parte de su propia base de código, y también ayuda al desarrollador que consume ese servicio para tener una visión clara de lo que la API del servicio se parece. Si el servicio no es una instancia de una class/interface de su propia base de código, siempre se debe considerar el uso de un prefijo para ello, de modo que se evitan las colisiones con otros servicios.

# Módulo devuelve un Array

Lo siguiente demuestra devuelve una matriz de configuración de un módulo de clase. Puede ser sustancialmente la misma que la configuración de la matriz del ejemplo anterior.

```sh
namespace SomeModule;

// you may eventually want to implement Zend\ModuleManager\Feature\ServiceProviderInterface
class Module
{
    public function getServiceConfig()
    {
        return array(
            'abstract_factories' => array(),
            'aliases' => array(),
            'factories' => array(),
            'invokables' => array(),
            'services' => array(),
            'shared' => array(),
        );
    }
}
```

# Creando una clase ServiceLocator-aware

Por defecto, el Marco de Zend MVC registra un inicializador que inyectará el ServiceManager por ejemplo, que es una implementación de [Zend\ServiceManager\ServiceLocatorInterface] , en cualquier clase de implementación [Zend\ServiceManager\ServiceLocatorAwareInterface].

Una aplicación sencilla parece a lo siguiente:

```sh
namespace SomeModule\Controller;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\DispatchableInterface as Dispatchable;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;

class BareController implements
    Dispatchable,
    ServiceLocatorAwareInterface
{
    protected $services;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }

    public function dispatch(Request $request, Response $response = null)
    {
        // ...

        // Retrieve something from the service manager
        $router = $this->getServiceLocator()->get('Router');

        // ...
    }
}
```